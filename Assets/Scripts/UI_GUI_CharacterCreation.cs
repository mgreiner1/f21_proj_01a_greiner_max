using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_GUI_CharacterCreation : MonoBehaviour
{
    public Player playerOne;

    public InputField strengthInput;
    public InputField dexInput;
    public InputField constInput;
    public InputField intInput;
    public InputField wisdInput;
    public InputField charisInput;


    public Text walkingSpeedText;
    public Text runningSpeedText;
    public Text jumpHeightText;

    public InputField jsontOutInputField;

    public Text raceToolTipText;
    public Text classToolTipText;


    private  List<string> raceList = new List<string>{"Select Race", "Dragonborn", "Dwarf", "Elf", "Gnome", "Half-elf", "Half-orc", "Halfling", "Human", "Triefling"};
    private List<string> raceDescription = new List<string> { "Select Race", "Your draconic heritage manifests in a variety of traits you share with other dragonborn.", "Your dwarf character has an assortment of in abilities, part and parcel of dwarven nature.",
        "Your elf character has a variety of natural abilities, the result of thousands of years of elven refinement.", "Your gnome character has certain characteristics in common with all other gnomes.",
        "Your half-elf character has some qualities in common with elves and some that are unique to half-elves.", "Your half-orc character has certain traits deriving from your orc ancestry.", "Your halfling character has a number of traits in common with all other halflings.",
        "It's hard to make generalizations about humans, but your human character has these traits.", "Tieflings share certain racial traits as a result of their infernal descent." };

    private List<string> classList = new List<string> { "Select Class", "Barbarian", "Bard", "Cleric", "Druid", "Fighter", "Monk", "Paladin", "Ranger", "Rogue","Sorcerer","Warlock","Wizard" };
    private List<string> classDescription = new List<string> { "Select Class", "In battle, you fight with primal ferocity. For some barbarians, rage is a means to an end�that end being violence.", "Whether singing folk ballads in taverns or elaborate compositions in royal courts, bards use their gifts to hold audiences spellbound.",
        "Clerics act as conduits of divine power.", "Druids venerate the forces of nature themselves. Druids holds certain plants and animals to be sacred, and most druids are devoted to one of the many nature deities.", 
        "Different fighters choose different approaches to perfecting their fighting prowess, but they all end up perfecting it.", "Coming from monasteries, monks are masters of martial arts combat and meditators with the ki living forces.",
        "Paladins are the ideal of the knight in shining armor; they uphold ideals of justice, virtue, and order and use righteous might to meet their ends.", "Acting as a bulwark between civilization and the terrors of the wilderness, rangers study, track, and hunt their favored enemies.",
        "Rogues have many features in common, including their emphasis on perfecting their skills, their precise and deadly approach to combat, and their increasingly quick reflexes.",
        "An event in your past, or in the life of a parent or ancestor, left an indelible mark on you, infusing you with arcane magic. As a sorcerer the power of your magic relies on your ability to project your will into the world.",
        "You struck a bargain with an otherworldly being of your choice: the Archfey, the Fiend, or the Great Old One who has imbued you with mystical powers, granted you knowledge of occult lore, bestowed arcane research and magic on you and thus has given you facility with spells.",
        "The study of wizardry is ancient, stretching back to the earliest mortal discoveries of magic. As a student of arcane magic, you have a spell book containing spells that show glimmerings of your true power which is a catalyst for your mastery over certain spells." };

   
    private List<string> alignmentList = new List<string> { "Select Alignment", "Lawful Good", "Neutral Good", "Lawful Neutral", "Neutral", "Chaotic Neutral", "Lawful Evil", "Neutral Evil", "Chaptoc Evil" };


    // Start is called before the first frame update
    void Start()
    {
        playerOne = ScriptableObject.CreateInstance <Player>();  
    }
    public void onCallBack_RollStrengthAbility()
    {
        int rand = OnCallBack_RollRandomAbility();
        int rolled = rand + 2;
        strengthInput.text = rolled.ToString();
        playerOne.abilityStrength = rolled;

    }
    public void onCallBack_RolDexterityAbility()
    {
        int rand = OnCallBack_RollRandomAbility();
        int rolled = rand + 2;
        dexInput.text = rolled.ToString();
        playerOne.abilityDexterity = rolled;

    }
    public void onCallBack_RollConstAbility()
    {
        int rand = OnCallBack_RollRandomAbility();
        int rolled = rand + 2;
        constInput.text = rolled.ToString();
        playerOne.abilityConstitution = rolled;

    }
    public void onCallBack_RollIntelligenceAbility()
    {
        int rand = OnCallBack_RollRandomAbility();
        int rolled = rand + 2;
        intInput.text = rolled.ToString();
        playerOne.abilityIntelligence = rolled;

    }
    public void onCallBack_RollWisdomAbility()
    {
        int rand = OnCallBack_RollRandomAbility();
        int rolled = rand + 2;
        wisdInput.text = rolled.ToString();
        playerOne.abilityWisdom = rolled;

    }
    public void onCallBack_RollCharismaAbility()
    {
        int rand = OnCallBack_RollRandomAbility();
        int rolled = rand + 2;
        charisInput.text = rolled.ToString();
        playerOne.abilityCharisma = rolled;

    }
    public void onCallBack_setRaceToolTip(int raceIndex)
    {
        raceToolTipText.text = raceDescription[raceIndex];
    }
    public void onCallBack_setClassToolTip(int classIndex)
    {
        classToolTipText.text = classDescription[classIndex];
    }
    public void setArmorClass(string armorClass)
    {
        int val;
        if (int.TryParse(armorClass, out val))
            playerOne.armorClass = Mathf.Clamp(val, 0, 100);
        else
            Debug.LogError("Could not convert String to Int or Value is not in range");
    }
    public void setCurrentXpPoints(string currentXpPoints)
    {
        int val;
        if (int.TryParse(currentXpPoints, out val))
            playerOne.xpPointsCurrent = val;
        else
            Debug.LogError("Could not convert String to Int");
    }
    public void setMaxXpPoints(string maxXpPoints)
    {
        int val;
        if (int.TryParse(maxXpPoints, out val))
            playerOne.xpPointsTotal = val;
        else
            Debug.LogError("Could not convert String to Int");
    }
    public void setCurrentHitPoints(string currentHitPoints)
    {
        int val;
        if (int.TryParse(currentHitPoints, out val))
            playerOne.hitPointCurrent = val;
        else
            Debug.LogError("Could not convert String to Int");
    }
    public void setMaxHitPoints(string maxHitPoints)
    {
        int val;
        if (int.TryParse(maxHitPoints, out val))
            playerOne.hitPointTotal = val;
        else
            Debug.LogError("Could not convert String to Int");
    }
    public void setName(string charName)
    {
        playerOne.playerName = charName;
    }
    public void setStrength(string Strength)
    {
        int val;
        if (int.TryParse(Strength, out val))
            playerOne.abilityStrength = val;
        else
            Debug.LogError("Could not convert String to Int");
    }

    public void setDexterity(string Dexterity)
    {
        int val;
        if (int.TryParse(Dexterity, out val))
            playerOne.abilityDexterity = val;
        else
            Debug.LogError("Could not convert String to Int");
    }
    public void setConstitution(string Consitution)
    {
        int val;
        if (int.TryParse(Consitution, out val))
            playerOne.abilityConstitution = val;
        else
            Debug.LogError("Could not convert String to Int");
    }
    public void setIntelligence(string Intelligence)
    {
        int val;
        if (int.TryParse(Intelligence, out val))
            playerOne.abilityIntelligence = val;
        else
            Debug.LogError("Could not convert String to Int");
    }
    public void setWisdom(string Wisdom)
    {
        int val;
        if (int.TryParse(Wisdom, out val))
            playerOne.abilityWisdom = val;
        else
            Debug.LogError("Could not convert String to Int");
    }
    public void setCharisma(string Charisma)
    {
        int val;
        if (int.TryParse(Charisma, out val))
            playerOne.abilityCharisma = val;
        else
            Debug.LogError("Could not convert String to Int");
    }
    public int OnCallBack_RollRandomAbility()
    {
        int numberD8rolls = 5;
        int numberD3rolls = 6; 

        List<int> listD8 = new List<int>();
        List<int> listD3 = new List<int>();

        for (int i = 0; i < numberD8rolls; i++)
        {
            listD8.Add(Random.Range(1, 8));
        }
        for (int i = 0; i < numberD3rolls; i++)
        {
            listD3.Add(Random.Range(1, 3));
        }

        List<int> totalList = new List<int>(); //combine both roll arrays

        totalList.AddRange(listD3);
        totalList.AddRange(listD8);
        totalList.Sort();
        totalList.Reverse(); //Sort list in Descending Order

        int diceRoll = totalList[0] + totalList[1] + totalList[2]; //Get the top three largest elemenet


        return diceRoll;
    }

    public void setRace(int raceIndex)
    {
        playerOne.race = raceList[raceIndex];
    }
    public void setClass(int ClassDNDIndex)
    {
        playerOne.classD = classList[ClassDNDIndex];
    }
    public void setAlignment(int alignmentIndex)
    {
        playerOne.alignment = alignmentList[alignmentIndex];
    }

    public void setWalkingSpeed(float walkingSpeed)
    {
        playerOne.speedWalking = (int) walkingSpeed;
        walkingSpeedText.text = "Walking Speed " + ((int) walkingSpeed).ToString();
    }
    public void setRunningSpeed(float runningSpeed)
    {
        playerOne.speedRunning = (int) runningSpeed;
        runningSpeedText.text = "Running Speed " + ((int) runningSpeed).ToString();
    }
    public void setJumpHeight(float jumpHeight)
    {
        playerOne.jumpHeight = (int) jumpHeight;
        jumpHeightText.text = "Jump Heigh " + ((int) jumpHeight).ToString();
    }

    public void OnCallBack_jsonTextOut()
    {
        jsontOutInputField.text = playerOne.SaveToString();
    }

    public void OnCallBack_ExitGame()
    {
        UnityEditor.EditorApplication.isPlaying = false;
        Application.Quit();
    }
}


