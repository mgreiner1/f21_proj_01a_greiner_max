using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : ScriptableObject
{


    public string playerName;

    public int abilityStrength;
    public int abilityDexterity;
    public int abilityConstitution;
    public int abilityIntelligence;
    public int abilityWisdom;
    public int abilityCharisma;

    public string race;
    public string classD;
    public string alignment;

    public int xpPointsCurrent;
    public int xpPointsTotal;
    public int hitPointCurrent;
    public int hitPointTotal;
    public int armorClass;
    public int speedWalking;
    public int speedRunning;
    public int jumpHeight;

    List<string> items = new List<string>();


    public string SaveToString()
    {
        return JsonUtility.ToJson(this);
    }
}
